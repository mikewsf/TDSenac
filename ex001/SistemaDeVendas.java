public class SistemaDeVendas {
    
    private MaquinaDeIngressos maquina;

    public void rodar() {
                
        maquina = new MaquinaDeIngressos(10, 200);
       
        maquina.consultarSaldo();

        maquina.comprarIngresso(10.5);

        maquina.comprarIngresso(5);
        
        maquina.consultarSaldo();

    }
    public static void main(String args[]){
        new SistemaDeVendas().rodar();
    }
}