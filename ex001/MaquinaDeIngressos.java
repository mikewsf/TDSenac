public class MaquinaDeIngressos{
    
    private double valor;
    
    private Ingresso ingresso;
    
    private int totalIngressosVendidos;
    
    private double saldo;
    
    private int totalIngressos;
    
    private String nomeDoEvento;
    
    public MaquinaDeIngressos(String nomeEvento, double valor, int totalIngressos){
        this.nomeDoEvento = nomeEvento;
        this.ingresso = new Ingresso(valor);
        this.totalIngressos = totalIngressos;
    }
    
    public void comprarIngresso(double valor){
        
        if(totalIngressos >= 1 && valor>= ingresso.get){
            
            double troco = valor - this.valor;
            saldo = saldo + this.valor;
            totalIngressos--;
            totalIngressosVendidos++;
            this.imprimirIngresso();
            if(troco > 0){
                System.out.println("Troco: "+troco);
            }
        }else{
            System.out.println("Sem troco");
            
        }
    }
    
    public void consultarSaldo(){
        System.out.println("Saldo: "+saldo);
    }
    
    private void imprimirIngresso(){
        System.out.println("Ingresso");
        System.out.println("--------");
        System.out.println("Valor: "+this.valor);
        System.out.println();
    }
}