public class Ingresso extends Object {
    private double valor;
    private int numero;
    
    public Ingresso(double valor) {
        this.valor = valor;
    }
    
    public void setValor(double valor) {
        this.valor = valor;
    }
    
    public double getValor() {
        return valor+5.0;
    }
    
    public String toString() {
        return  "Ingresso\n"+     
                "--------\n"+
                "Evento: \n"+
                "Valor: "+this.getValor();
    }
}