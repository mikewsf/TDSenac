public class SistemaDeVendas {
    
    private MaquinaDeIngressos maquina;

    public void rodar() {
                
        maquina = new MaquinaDeIngressos("Cirque du Soleil",10, 2);
       
        //Caso de Uso [Gerência]: Consultar saldo
        maquina.consultarSaldo();

        //Caso de Uso [Público]: Comprar ingresso
        maquina.comprarIngresso(10.5);
        // Fim Comprar ingresso

        //Caso de Uso [Público]: Comprar ingresso
        maquina.comprarIngresso(5);
        // Fim Comprar ingresso
        
        //Caso de Uso [Público]: Comprar ingresso
        maquina.comprarIngresso(20);
        // Fim Comprar ingresso

        //Caso de Uso [Público]: Comprar ingresso
        maquina.comprarIngresso(10);
        // Fim Comprar ingresso


        //Caso de Uso [Gerência]: Consultar saldo
        maquina.consultarSaldo();

        maquina.imprimirRelatório();

    }
    public static void main(String args[]){
        new SistemaDeVendas().rodar();
    }
}
