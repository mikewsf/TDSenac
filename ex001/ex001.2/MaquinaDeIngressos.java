public class MaquinaDeIngressos {
    
    //Ingresso vendido no momento
    private Ingresso ingresso;
    //Total de ingressos vendidos
    private int totalIngressosVendidos;
    //Valor total das vendas
    private double saldo;
    //Total de ingressos à venda
    private int totalIngressos;
    // Nome do evento
    private String nomeDoEvento;

    public MaquinaDeIngressos(String nomeEvento, double valor, int totalIngressos) {
        this.nomeDoEvento = nomeEvento;
        this.ingresso = new Ingresso(valor);       
        this.totalIngressos = totalIngressos;
    }

    public void comprarIngresso(double valor) {
        //Se o valor recebido for suficiente
        //para comprar um ingresso, vende o ingresso

        if(totalIngressos>=1 && valor >= ingresso.getValor()) {
            //calcular troco
            double troco = valor - ingresso.getValor();
            saldo = saldo + ingresso.getValor();
            totalIngressos--;
            totalIngressosVendidos++;
            this.imprimirIngresso();
            if(troco > 0) {
                System.out.println("Troco: "+troco);
            }
        } else {
            System.out.println("Valor insuficiente ou ingressos esgotados. ");
            System.out.println("Valor do ingresso = " + ingresso.getValor());
        }
    }

    public void consultarSaldo() {
        System.out.println("Saldo = "+saldo);
    }

    private void imprimirIngresso() {
        System.out.println(ingresso);
    }

    public void imprimirRelatório() {
        System.out.println("Relatório de vendas");
        System.out.println("-------------------");
        System.out.println("Evento: "+nomeDoEvento);
        System.out.println("Total de ingressos vendidos: "+ totalIngressosVendidos);
        System.out.println("Valor do ingresso: "+ ingresso.getValor());
        System.out.println("Saldo: "+saldo);
    }
}

